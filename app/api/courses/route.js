import { NextResponse } from 'next/server'
import courses from './data.json'

export async function GET(request) {
    return NextResponse.json(courses)
}



export async function POST(request) {
    const { id, title, link, description, level } = await request.json()
    console.log(title, link, description, level)
    const newCourse = {
        id, title, link, description, level
    }
    courses.push(newCourse)
    return NextResponse.json(courses)
}