import { NextResponse } from "next/server"
import courses from '../data.json'

export async function GET(request) {
    const { searchParams } = new URL(request.url)
    // console.log(searchParams)
    const query = searchParams.get('query')
    const filterCourses = courses.filter(course => course.title.toLowerCase().includes(query.toLocaleLowerCase()))
    return NextResponse.json(filterCourses)
}