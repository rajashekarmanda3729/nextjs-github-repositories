import React from 'react'

const About = () => {
  return (
    <div className='row'>
      <div className="col-12 d-flex flex-column justify-content-center align-items-center p-5 mb-5">
        <h1>About</h1>
        <p className='fs-5'>Lorem ipsum dolor sit amet consectetur adipisicing elit.
          Nesciunt libero natus ducimus rerum ratione quam modi eaque velit atque praesentium,
          consequatur deserunt sapiente, temporibus sequi necessitatibus voluptatibus quaerat voluptatem,
          voluptate amet aspernatur? Est, quisquam rerum inventore assumenda voluptatibus eligendi animi?
          Doloremque dolorem consequuntur debitis maiores amet dolor, ipsam blanditiis incidunt.</p>
      </div>
    </div>
  )
}

export default About