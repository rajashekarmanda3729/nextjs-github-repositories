import RepoDetails from "@/app/components/RepoDetails"
import RepoDirectories from "@/app/components/RepoDirectories"
import Link from "next/link"
import { Suspense } from "react"

const RepoInfo = ({ params }) => {
    return (
        <div className="row">
            <div className="d-flex border-warning justify-content-center col-12 mt-4">
                <div className="d-flex col-8 flex-column border bg-light text-dark rounded-4 p-4">
                    <Link href={`/code/repos`}>
                        <button className="btn btn-dark w-25">Back to Repos</button>
                    </Link>
                    <Suspense fallback={<h1>Loading...repo</h1>}>
                        <RepoDetails name={params.name} />
                    </Suspense>
                    <Suspense fallback={<h1>Loading...Directories</h1>}>
                        <RepoDirectories name={params.name} />
                    </Suspense>
                </div>
            </div>
        </div>
    )
}

export default RepoInfo