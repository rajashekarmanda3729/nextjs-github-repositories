import Repository from "@/app/components/Repository"

async function fetchRepos() {
    const response = await fetch('https://api.github.com/users/bradtraversy/repos', {
        next: {
            revalidate: 60
        }
    })
    const data = await response.json()
    return data
}

const ReposPage = async () => {
    const reposData = await fetchRepos()

    return (
        <div className="container-fluid">
            <h1>Repositories</h1>
            <div className="row">
                <div className="d-flex flex-column justify-content-center align-items-center">
                    {
                        reposData.length > 0 && reposData.map(repo => {
                            return <Repository key={repo.name} repo={repo} />
                        })
                    }
                </div>
            </div>
        </div >
    )
}

export default ReposPage