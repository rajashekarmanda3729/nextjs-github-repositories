import Header from "./components/Header"
import 'bootstrap/dist/css/bootstrap.css'
import './globals.css'

export const metadata = {
  title: 'NextJS Project',
  description: 'Generated by create next app',
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body>
        <div className="container-fluid">
          <Header />
          {children}
        </div>
      </body>
    </html>
  )
}
