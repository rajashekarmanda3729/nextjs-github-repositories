'use client'

import { TailSpin } from "react-loader-spinner"

const Loading = () => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-12 d-flex justify-content-center align-items-center">
                    <TailSpin color="red" />
                </div>
            </div>
        </div>
    )
}

export default Loading