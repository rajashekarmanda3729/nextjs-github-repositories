"use client"
import { useEffect, useState } from 'react'

const SearchCourse = ({ setCourses }) => {

    const [query, setQuery] = useState('')

    // const handleSubmit = async (e) => {
    //     e.preventDefault()
    //     const response = await fetch(`/api/courses/search?query=${query}`)
    //     const data = await response.json()
    //     setCourses(data)
    // }

    useEffect(() => {
        const getCourse = async () => {
            const response = await fetch(`/api/courses/search?query=${query}`)
            const data = await response.json()
            setCourses(data)
        }
        getCourse()
    }, [query])

    return (
        <form className='w-50 d-flex justify-content-between align-items-center mb-4 mt-4'>
            <input type="text"
                onChange={(e) => setQuery(e.target.value)}
                value={query}
                placeholder='Search Course...'
                className='w-100 rounded-2 p-2 border-0 fs-4 text-warning' />
            {/* <button className="btn btn-primary" type='submit'>Search</button> */}
        </form >
    )
}

export default SearchCourse