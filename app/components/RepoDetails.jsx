import React from 'react'
import axios from 'axios'
import { FaStar, FaCodeBranch, FaEye } from 'react-icons/fa'

async function getRepoData(name) {
    const response = await fetch(`https://api.github.com/repos/bradtraversy/${name}`, {
        next: {
            revalidate: 60
        }
    })
    const data = response.json()
    return data
}

const RepoDetails = async ({ name }) => {
    const repoData = await getRepoData(name)

    return (
        <>
            <h1>{repoData.name}</h1>
            <p>{repoData.description}</p>
            <div className="d-flex justify-content-between align-items-center p-1">
                <h5 className='d-flex'><FaStar /> {repoData.stargazers_count}</h5>
                <h5><FaCodeBranch /> {repoData.forks_count}</h5>
                <h5><FaEye /> {repoData.watchers_count}</h5>
            </div>
        </>
    )
}

export default RepoDetails