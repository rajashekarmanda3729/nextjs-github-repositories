
async function getRepoDirectories(name) {
    const response = await fetch(`https://api.github.com/repos/bradtraversy/${name}/contents`, {
        next: {
            revalidate: 60
        }
    })
    const data = response.json()
    return data
}


const RepoDirectories = async ({ name }) => {
    const repoDirectories = await getRepoDirectories(name)
    // console.log(repoDirectories)
    return (
        <div>
            <h4>Directories</h4>
            <ul>
                {
                    repoDirectories.map(repo => {
                        return repo.type == 'dir' && <li className="text-success">{repo.name}</li>
                    })
                }
            </ul>
        </div>
    )
}

export default RepoDirectories