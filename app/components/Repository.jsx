import { FaStar, FaCodeBranch, FaEye } from 'react-icons/fa'
import Link from 'next/link'
import '../globals.css'

const Repository = (props) => {
    return (
        <div className='col-8'>
            <Link href={`/code/repos/${props.repo.name}`} className='text-decoration-none'>
                <div className="d-flex flex-column repo-card mt-4 p-3 m-1 text-dark shadow rounded-4">
                    <h1>{props.repo.name}</h1>
                    <p>{props.repo.description}</p>
                    <div className="d-flex justify-content-between align-items-center p-1">
                        <h5 className='d-flex'><FaStar color='green' /> {props.repo.stargazers_count}</h5>
                        <h5><FaCodeBranch color='red' /> {props.repo.forks_count}</h5>
                        <h5><FaEye color='pink' /> {props.repo.watchers_count}</h5>
                    </div>
                </div>
            </Link>
        </div>
    )
}
export default Repository