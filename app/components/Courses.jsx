import Link from "next/link"

function Courses({ coursesData }) {

    return (

        <div className="d-flex flex-column">
            {
                coursesData && coursesData.map(course => {
                    return <div className="d-flex flex-column repo-card m-3 p-3 rounded-4" key={course.id}>
                        <h1 className="text-dark">{course.title}<span className="fs-4">{` (${course.level})`}</span></h1>
                        <h6 className="text-dark">{course.description}</h6>
                        <Link href={course.link} target="_blank">
                            <button className="btn btn-primary">{`@Go To Course`}</button >
                        </Link>
                    </div>
                })
            }
        </div>
    )
}
export default Courses