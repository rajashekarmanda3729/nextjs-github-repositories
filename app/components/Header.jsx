import Link from "next/link"

const Header = () => {
    return (
        <div className='row'>
            <div className="col-12 bg-warning">
                <div className="d-flex p-3 justify-content-between align-items-center">
                    <h2 className="col-8 text-light">#Traversy Media</h2>
                    <div className="d-flex col-4 justify-content-evenly align-items-center">
                        <Link href='/' className="text-light text-decoration-none">
                            <h3 >Home</h3>
                        </Link>
                        <Link href='/about' className="text-light text-decoration-none">
                            <h3>About</h3>
                        </Link>
                        <Link href='/code/repos' className="text-light text-decoration-none">
                            <h3>Code</h3>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Header