"use client"
import { useState, useEffect } from "react"
import Courses from "./components/Courses"
import axios from "axios"
import Loading from './loading'
import SearchCourse from "./components/SearchCourse"
import './globals.css'

function Home() {

  const [courses, setCourses] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    async function getCourses() {
      const response = await axios('/api/courses')
      setCourses(response.data)
      setIsLoading(false)
    }
    getCourses()
  }, [])

  if (isLoading) {
    return <Loading />
  }

  return (
    <div className='row'>
      <div className="d-flex flex-column justify-content-center align-items-center p-4">
        <h1>Courses</h1>
        <SearchCourse setCourses={setCourses} />
        {
          courses.length > 0 ?
            <Courses coursesData={courses} /> :
            <h1 className="text-light">No Courses Found</h1>
        }
      </div>
    </div>
  )
}
export default Home